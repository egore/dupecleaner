package de.egore911.dupecleaner;

import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Christoph Brill &lt;christoph.brill@cgm.com&gt;
 * @since 26.05.2017 21:34
 */
public class Main {

    private static class Producer implements Runnable {

        private final BlockingQueue<Path> files;
        private final String[] args;
        private AtomicBoolean completed;

        private Producer(BlockingQueue<Path> files, String[] args, AtomicBoolean completed) {
            this.files = files;
            this.args = args;
            this.completed = completed;
        }

        @Override
        public void run() {
            for (String path : args) {
                try {
                    listFiles(files, Paths.get(path));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            System.err.println("Found " + files.size() + " candidates");
            completed.set(true);
        }

        private static void listFiles(Collection<Path> files, Path path) throws IOException {
            if (!Files.isReadable(path)) {
                return;
            }
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
                for (Path entry : stream) {
                    if (Files.isDirectory(entry)) {
                        listFiles(files, entry);
                    } else {
                        if (Files.isReadable(entry)) {
                            files.add(entry);
                        }
                    }
                }
            }
        }
    }

    private static class Consumer implements Runnable {

        private final BlockingQueue<Path> files;
        private AtomicBoolean completed;
        private final MessageDigest md;
        private final Map<String, Collection<Path>> dupes;

        private Consumer(BlockingQueue<Path> files, AtomicBoolean completed) {
            this.files = files;
            this.completed = completed;
            try {
                md = MessageDigest.getInstance("SHA-1");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
            dupes = new HashMap<>();
        }

        @Override
        public void run() {
            int count = 0;

            Path file;
            try {
                while (((file = files.poll(1, TimeUnit.SECONDS)) != null) || !completed.get()) {

                    if (file == null) {
                        continue;
                    }

                    md.reset();

                    if (count++ % 99 == 0 || count == files.size()) {
                        System.err.println(count);
                    }

                    try {
                        md.update(Files.readAllBytes(file));
                    } catch (OutOfMemoryError e) {
                        System.err.println(file + " is to large");
                    } catch (IOException e) {
                        System.err.println("Could not read file " + file);
                    }
                    String hex = Hex.encodeHexString(md.digest());
                    Collection<Path> paths = dupes.computeIfAbsent(hex, k -> new ArrayList<>());
                    paths.add(file);


                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.err.println(files.size());

            dupes.values().stream().filter(l -> l.size() > 1).forEach(System.err::println);
        }

        Map<String, Collection<Path>> getDupes() {
            return dupes;
        }
    }

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        if (args.length == 0) {
            return;
        }

        AtomicBoolean completed = new AtomicBoolean(false);
        BlockingQueue<Path> files = new LinkedBlockingDeque<>();

        Producer producer = new Producer(files, args, completed);
        new Thread(producer).start();

        Consumer consumer = new Consumer(files, completed);
        new Thread(consumer).start();

    }

}
