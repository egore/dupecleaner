## [1.0.3](https://gitlab.com/appframework/egore-personal/dupecleaner/compare/1.0.2...1.0.3) (2025-02-26)

## [1.0.2](https://gitlab.com/appframework/egore-personal/dupecleaner/compare/1.0.1...1.0.2) (2025-01-28)


### Bug Fixes

* **deps:** update dependency commons-codec:commons-codec to v1.18.0 ([54c374a](https://gitlab.com/appframework/egore-personal/dupecleaner/commit/54c374aae8cbee02ae9baf084008912c88b3db0d))

## [1.0.1](https://gitlab.com/appframework/egore-personal/dupecleaner/compare/1.0.0...1.0.1) (2025-01-08)


### Bug Fixes

* **deps:** update dependency commons-codec:commons-codec to v1.17.2 ([551e3ec](https://gitlab.com/appframework/egore-personal/dupecleaner/commit/551e3ecab22475e512f14c7824c477e0b62c5487))

# 1.0.0 (2024-10-29)


### Bug Fixes

* **ci:** Add dummy distributionManagement ([2f68173](https://gitlab.com/appframework/egore-personal/dupecleaner/commit/2f6817394cadaa17624693b98380d090fa0c77d6))
* **deps:** update dependency commons-codec:commons-codec to v1.17.0 ([f4ad13e](https://gitlab.com/appframework/egore-personal/dupecleaner/commit/f4ad13e82a5a0d8fec7a7607edf4db1d9b903966))
* **deps:** update dependency commons-codec:commons-codec to v1.17.1 ([bb66d23](https://gitlab.com/appframework/egore-personal/dupecleaner/commit/bb66d2364cfa2f61edfd68a0ad016abaffd4196d))
